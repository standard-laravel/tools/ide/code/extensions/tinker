<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->commands(SketchpadCommand::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        if (!$this->app->runningInConsole()) {
            return;
        }
        $this->commands([
            \App\Providers\SketchpadCommand::class,
        ]);
    }

    public function provides()
    {
        return ['command.tinker'];
    }
}

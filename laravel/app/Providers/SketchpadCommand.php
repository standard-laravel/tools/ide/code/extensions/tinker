<?php

namespace App\Providers;

use Psy\Shell;
use Throwable;
use Psy\Configuration;
use Illuminate\Support\Env;
use Psy\VersionUpdater\Checker;
use Psy\CodeCleaner\NoReturnValue;
use Laravel\Tinker\ClassAliasAutoloader;
use Psy\Command\Command;
use Symfony\Component\Console\Input\InputOption;

class SketchpadCommand extends \Laravel\Tinker\Console\TinkerCommand
{
    protected $name = 'sketchpad';

    public function handle()
    {
        $this->getApplication()->setCatchExceptions(false);

        $shell = $this->prepareShell();

        $loader = with(
            $this->getLaravel()->make('config'),
            fn ($config) => ClassAliasAutoloader::register($shell, $this->getPath(), $config->get('tinker.alias', []), $config->get('tinker.dont_alias', []))
        );

        return match(!!$this->option('execute') || !!$this->option('evaluate')) {
            true => $this->handleExecute($shell, $loader),
            default => $this->handleRepl($shell, $loader),
        };
    }

    protected function handleExecute(Shell $shell, ClassAliasAutoloader $loader)
    {
        $shellConfig = with(
            new \ReflectionObject($shell),
            fn($reflection) => $reflection->getProperty('config')->getValue($shell)
        );

        $code = $this->option('execute') ?? $this->option('evaluate');

        try {
            $result = $shell->execute($code);

            if ($this->option('evaluate')) {
                $this->line(
                    $result instanceof NoReturnValue ? '' : $shellConfig->getPresenter()->present($result)
                );
            }
        } catch (Throwable $e) {
            $shell->writeException($e);

            return Command::FAILURE;
        } finally {
            $loader->unregister();
        }

        return Command::SUCCESS;
    }

    protected function handleRepl(Shell $shell, ClassAliasAutoloader $loader)
    {
        try {
            return $shell->run();
        } finally {
            $loader->unregister();
        }
    }

    protected function prepareShell(): Shell
    {
        return tap(
            new Shell($this->prepareShellConfig()),
            function (Shell $shell) {
                $shell->addCommands($this->getCommands());
                $shell->setIncludes($this->argument('include'));
                $shell->setOutput($this->output);
            }
        );
    }

    protected function prepareShellConfig(): Configuration
    {
        return tap(
            Configuration::fromInput($this->input),
            function (Configuration $config) {
                $config->setUpdateCheck(Checker::NEVER);
                if ($this->option('execute') || $this->option('evaluate')) {
                    $config->setRawOutput(true);
                }
                // This reads better, can we safely do it?
                // $config->setRawOutput(
                //     (bool) ($this->option('execute') || $this->option('evaluate'))
                // );
                $config->getPresenter()->addCasters($this->getCasters());
            }
        );
    }

    protected function getPath()
    {
        return with(
            Env::get('COMPOSER_VENDOR_DIR', $this->getLaravel()->basePath().DIRECTORY_SEPARATOR.'vendor'),
            fn ($path) => $path .= '/composer/autoload_classmap.php'
        );
    }

    protected function getOptions()
    {
        return [
            ...parent::getOptions(),
            ['evaluate', null, InputOption::VALUE_OPTIONAL, 'Evaluate the given code using Tinker'],
        ];
    }
}

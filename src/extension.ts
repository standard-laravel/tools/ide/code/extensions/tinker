import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';

const util = require('util')

let monitor: any = null;

export function activate(context: vscode.ExtensionContext) {
    registerCommands(context);
}

// TODO: Add second command to re-run previous execute
// TODO: Bind both commands to a keybinding
function registerCommands(context: vscode.ExtensionContext) {
    context.subscriptions.push(
        vscode.commands.registerCommand('sketchpad.execute', async () => {
            const executable = task(await file());
            if (executable) {
                vscode.tasks.executeTask(executable);
            }
        })
    );
}

function task(path: string | undefined) {
    if (!path) {
        vscode.window.showErrorMessage('No valid PHP file selected');
        return undefined;
    }
    const commandBuilder = new String().concat;

    const task = new vscode.Task(
        { type: 'sketchpad', task: 'execute' },
        vscode.TaskScope.Workspace,
        'execute',
        'sketchpad',
        new vscode.ShellExecution(
            `php artisan tinker --execute='$____sketchpad = fn () => include "${path}"; $____sketchpad();'`
        ),
        '$sketchpad',
    );

    task.presentationOptions = {
        clear: true,
        echo: false,
        showReuseMessage: false,
    };

    return task;
}

async function file(): Promise<string | undefined> {
    return await activeFile();
}

async function activeFile(): Promise<string | undefined> {
    const directory = await ensureDirectoryExists();
    if (!directory) return undefined;

    const editor = vscode.window.activeTextEditor;

    try {
        // TODO: Is there anything we can do to avoid passing editor around?
        confirmActiveEditor(editor);
        confirmActiveEditorIsFile(editor);
        confirmActiveFileIsPhp(editor);
        confirmActiveFileIsIn(editor, directory);

        return await editor?.document.fileName;
    } catch (error: any) {
        vscode.window.showWarningMessage(error.message);
        return await promptForFile(directory);
    }
}

async function ensureDirectoryExists(): Promise<string | undefined> {
    const folders = vscode.workspace.workspaceFolders;

    if (!folders) {
        vscode.window.showErrorMessage('No workspace folder found');
        return undefined;
    }

    const directory = path.join(folders[0].uri.fsPath, 'sketchpad/laravel');

    try {
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        return directory;
    } catch (error) {
        vscode.window.showErrorMessage(`Failed to create sketchpad directory: ${error}`);
        return undefined;
    }
}

function confirmActiveEditor(editor: vscode.TextEditor | undefined): void {
    if (!editor) throw new Error('No active editor');
}

function confirmActiveEditorIsFile(editor: vscode.TextEditor | undefined): void {
    if (editor?.document.uri.scheme !== 'file') throw new Error('Active editor is not a file');
}

function confirmActiveFileIsPhp(editor: vscode.TextEditor | undefined): void {
    if (editor?.document.languageId !== 'php') throw new Error('Active file is not a PHP file');
}

function confirmActiveFileIsIn(editor: vscode.TextEditor | undefined, directory: string): void {
    const normalizedFilePath = path.normalize(editor?.document.fileName ?? '');
    const normalizedDirectory = path.normalize(directory);

    if (!normalizedFilePath.startsWith(normalizedDirectory)) throw new Error('Active file is not in the sketchpad directory');
}

async function promptForFile(directory: string): Promise<string | undefined> {
    vscode.window.showInformationMessage('Please select a PHP file from the sketchpad directory');

    try {
        const files = runnableFiles(directory);
        let filePath;

        if (files.length === 0) {
            await createSketchpad(directory);
            return await promptForFile(directory);
        }

        // If there's only one file, return it directly without prompting
        if (files.length === 1) {
            filePath = files[0].filePath;
        } else {
            const selectedFile = await vscode.window.showQuickPick(files, {
                placeHolder: 'Select a file to execute'
            });

            filePath = selectedFile ? selectedFile.filePath : undefined;
        }

        if (filePath) {
            // Open the new file in editor
            const document = await vscode.workspace.openTextDocument(filePath);
            const editor = await vscode.window.showTextDocument(document);

            // Move cursor to the end of the file
            const lastLine = document.lineCount - 1;
            const lastCharPosition = document.lineAt(lastLine).text.length;
            editor.selection = new vscode.Selection(lastLine, lastCharPosition, lastLine, lastCharPosition);
            editor.revealRange(new vscode.Range(lastLine, 0, lastLine, lastCharPosition));
        }

        return filePath;
    } catch (error) {
        return await createSketchpad(directory);
    }
}

async function createSketchpad(directory: string): Promise<string | undefined> {
    try {
        const filePath = path.join(directory, 'sketchpad.php');

        // Create file with PHP opening tag, import and sample code
        fs.writeFileSync(filePath, "<?php\n\nreturn 'Silence in the face of evil is itself evil.';");

        return filePath;
    } catch (error) {
        vscode.window.showErrorMessage(`Failed to create new sketchpad file: ${error}`);
        return undefined;
    }
}

function runnableFiles(directory: string) {
    return fs.readdirSync(directory)
        .filter(file => file.endsWith('.php'))
        .map(file => ({
            label: file,
            description: 'Laravel Sketchpad',
            filePath: path.join(directory, file)
        }));
}

module.exports = {
    activate,
};

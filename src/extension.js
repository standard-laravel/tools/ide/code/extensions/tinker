"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.activate = void 0;
const vscode = require("vscode");
const fs = require("fs");
const path = require("path");
let monitor = null;
function activate(context) {
    registerCommands(context);
}
exports.activate = activate;
// TODO: Add second command to re-run previous execute
// TODO: Bind both commands to a keybinding
function registerCommands(context) {
    context.subscriptions.push(vscode.commands.registerCommand('tinker.execute', async () => {
        const executable = task(await file());
        if (executable) {
            vscode.tasks.executeTask(executable);
        }
    }));
}
function task(path) {
    if (!path) {
        vscode.window.showErrorMessage('No valid PHP file selected');
        return undefined;
    }
    const task = new vscode.Task({ type: 'tinker', task: 'execute' }, vscode.TaskScope.Workspace, 'execute', 'tinker', new vscode.ShellExecution(`php addddddrtisan tinker --evaluate='$test = fn () => require "${file}"; return $test();'`), '$tinker');
    task.presentationOptions = {
        clear: true,
        echo: false,
        showReuseMessage: false,
    };
    return task;
}
async function file() {
    return await activeFile();
}
async function activeFile() {
    const directory = await ensureDirectoryExists();
    if (!directory)
        return undefined;
    const editor = vscode.window.activeTextEditor;
    try {
        // TODO: Is there anything we can do to avoid passing editor around?
        confirmActiveEditor(editor);
        confirmActiveEditorIsFile(editor);
        confirmActiveFileIsPhp(editor);
        confirmActiveFileIsIn(editor, directory);
        return await editor?.document.fileName;
    }
    catch (error) {
        vscode.window.showWarningMessage(error.message);
        return await promptForFile(directory);
    }
}
async function ensureDirectoryExists() {
    const folders = vscode.workspace.workspaceFolders;
    if (!folders) {
        vscode.window.showErrorMessage('No workspace folder found');
        return undefined;
    }
    const directory = path.join(folders[0].uri.fsPath, 'scratchpad/laravel');
    try {
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        return directory;
    }
    catch (error) {
        vscode.window.showErrorMessage(`Failed to create scratchpad directory: ${error}`);
        return undefined;
    }
}
function confirmActiveEditor(editor) {
    if (!editor)
        throw new Error('No active editor');
}
function confirmActiveEditorIsFile(editor) {
    if (editor?.document.uri.scheme !== 'file')
        throw new Error('Active editor is not a file');
}
function confirmActiveFileIsPhp(editor) {
    if (editor?.document.languageId !== 'php')
        throw new Error('Active file is not a PHP file');
}
function confirmActiveFileIsIn(editor, directory) {
    const normalizedFilePath = path.normalize(editor?.document.fileName ?? '');
    const normalizedScratchpadDir = path.normalize(directory);
    if (!normalizedFilePath.startsWith(normalizedScratchpadDir))
        throw new Error('Active file is not in the scratchpad directory');
}
async function promptForFile(scratchpadDir) {
    vscode.window.showInformationMessage('Please select a PHP file from the scratchpad directory');
    try {
        const files = runnableFiles(scratchpadDir);
        if (files.length === 0) {
            vscode.window.showWarningMessage('No PHP files found in the scratchpad directory');
            return undefined;
        }
        const selectedFile = await vscode.window.showQuickPick(files, {
            placeHolder: 'Select a PHP file to execute'
        });
        return selectedFile ? selectedFile.filePath : undefined;
    }
    catch (error) {
        vscode.window.showErrorMessage(`Error listing PHP files: ${error}`);
        return undefined;
    }
}
function runnableFiles(scratchpadDir) {
    return fs.readdirSync(scratchpadDir)
        .filter(file => file.endsWith('.php'))
        .map(file => ({
        label: file,
        description: 'PHP Scratchpad File',
        filePath: path.join(scratchpadDir, file)
    }));
}
module.exports = {
    activate,
};
//# sourceMappingURL=extension.js.map